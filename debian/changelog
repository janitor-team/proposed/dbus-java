dbus-java (2.8-10) unstable; urgency=medium

  * Switch to debhelper-compat = 13.
  * Declare compliance with Debian Policy 4.5.1.
  * Drop dbus-java-bin because it is of little value for Debian users. The
    package does not work as intended anymore. Upstream development has stopped.
    (Closes: #963960, #631670, #777241)

 -- Markus Koschany <apo@debian.org>  Sun, 06 Dec 2020 00:47:22 +0100

dbus-java (2.8-9) unstable; urgency=medium

  * Team upload.
  * Update no-translations.patch to ignore resources bundle no longer
    being built. (Closes: #893208)
  * Use debhelper 11.
  * Correct spelling error (according to lintian) in description
  * Bump Standards-Version to 4.1.3
  * Use https URL for Homepage

 -- tony mancill <tmancill@debian.org>  Sun, 18 Mar 2018 20:36:18 -0700

dbus-java (2.8-8) unstable; urgency=medium

  * Do not suggest libdbus-java-doc anymore.
  * Remove default-jdk-doc from Build-Depends.

 -- Markus Koschany <apo@debian.org>  Thu, 21 Dec 2017 13:20:11 +0100

dbus-java (2.8-7) unstable; urgency=medium

  * Remove Matthew Johnson from Uploaders because he is not active anymore.
    Add myself to Uploaders to comply with the Debian Policy.
    (Closes: #762550)
  * Switch to compat level 10.
  * wrap-and-sort -sa.
  * Use canonical VCS address.
  * Remove trailing whitespace.
  * Compile for Java 7.
  * Drop libdbus-java-doc.README.Debian.
  * Declare compliance with Debian Policy 4.1.2.
  * Depend on a Java 7 runtime at least.
  * Add no-translations.patch and do not use msgfmt anymore.
    Apparently the JAVAC variable is set but msgfmt fails because of another
    yet unknown error. The only available translation is en_GB. I guess we can
    skip that. (Closes: #875355)
  * Remove gettext from Build-Depends.
  * Do not build the documentation anymore to work around the FTBFS with Java 9.
    dbus-java is unmaintained upstream. It is only a matter of time when no
    other package depends on it.

 -- Markus Koschany <apo@debian.org>  Thu, 21 Dec 2017 00:10:55 +0100

dbus-java (2.8-6) unstable; urgency=medium

  * Team upload.
  * debian/control:
    - Depend on default-jre-headless | java6-runtime-headless
      instead of openjdk-{6,7}-jre (Closes: #503815)
    - Fixed a typo in the name of the maintainer
    - Standards-Version updated to 3.9.6 (no changes)
    - Removed the duplicate Section field for libdbus-java

 -- Emmanuel Bourg <ebourg@apache.org>  Fri, 13 Feb 2015 23:02:32 +0100

dbus-java (2.8-5) unstable; urgency=medium

  * Team upload.
  * Update Standards-Version to 3.9.5 (no changes).
  * debian/rules:
    - Address jh_manifest warning. (Closes: #619888, LP: #663277)
    - No longer use bzip2 compression for binary deb.
    - Set javac -source and -target options to 1.6.

 -- tony mancill <tmancill@debian.org>  Fri, 31 Jan 2014 20:42:48 -0800

dbus-java (2.8-4) unstable; urgency=low

  * Team upload.
  * Correct maintainer mail address.  (Closes: #679031)

 -- Niels Thykier <niels@thykier.net>  Sat, 30 Jun 2012 18:47:52 +0200

dbus-java (2.8-3) unstable; urgency=low

  * Team upload.
  * Bump build dependency on javahelper to allow OpenJDK-7 as
    alternative to OpenJDK-6.
  * Remove stamp file in clean rule.

 -- Niels Thykier <niels@thykier.net>  Tue, 19 Jun 2012 12:00:13 +0200

dbus-java (2.8-2) unstable; urgency=low

  * Team upload.

  [ Matthew Johnson ]
  * Move to a team-maintained package (moving to java team not utopia)

  [ Damien Raude-Morvan ]
  * Switch to default-jdk (Closes: #642682).
  * Bump Standards-Version to 3.9.2: add Homepage.
  * Switch to 3.0 (quilt) format.

 -- Damien Raude-Morvan <drazzib@debian.org>  Mon, 21 Nov 2011 00:12:26 +0100

dbus-java (2.8-1) unstable; urgency=low

  * New upstream release (fixes some array serialization issues)

 -- Matthew Johnson <mjj29@debian.org>  Thu, 05 Aug 2010 19:48:13 +0000

dbus-java (2.7-2) unstable; urgency=low

  * Change from classpath-doc to default-jdk-doc (Closes: #567271)
  * fix some lintian warnings
  * dh_compress seems to be compressing the pdf, but evince copes with this, so
    lets just list it in doc-base compressed

 -- Matthew Johnson <mjj29@debian.org>  Sat, 20 Mar 2010 13:33:15 +0000

dbus-java (2.7-1) unstable; urgency=low

  * New Upstream Release
  * Convert to dh 7
  * Remove dependencies on locales

 -- Matthew Johnson <mjj29@debian.org>  Sun, 06 Dec 2009 11:10:53 +0000

dbus-java (2.6-1) unstable; urgency=low

  * New Upstream Release
  * Bump Standards-Version
  * Change to section java

 -- Matthew Johnson <mjj29@debian.org>  Sun, 05 Apr 2009 11:21:47 +0100

dbus-java (2.5-4) unstable; urgency=medium

  * Depend on texlive-latex-recommended (Closes: #494212)

 -- Matthew Johnson <mjj29@debian.org>  Fri, 08 Aug 2008 15:43:24 +0100

dbus-java (2.5-3) unstable; urgency=low

  * javahelper should have fixed the depends on libmatthew-debug-java....
    except it wasn't that problem. Missing dependency added to jar in
    debian/rules
    (Really Closes: #491208)
  * Fixed changelog entry below to not be on crack.

 -- Matthew Johnson <mjj29@debian.org>  Mon, 21 Jul 2008 11:26:17 +0100

dbus-java (2.5-2) unstable; urgency=low

  * Change to depend on openjdk and move to main from contrib
    (Closes: #490812, #491349)
  * javahelper should have fixed the depends on libmatthew-debug-java.
    (Closes: #491208)

 -- Matthew Johnson <mjj29@debian.org>  Sun, 20 Jul 2008 22:55:52 +0100

dbus-java (2.5-1) unstable; urgency=low

  * New Upstream Release

 -- Matthew Johnson <mjj29@debian.org>  Tue, 24 Jun 2008 23:49:24 +0100

dbus-java (2.4-2) unstable; urgency=low

  * Fix depends/wrapper script JVM mismatch (Closes: #473569)
  * Ensure correct dependency on lidbus-java in dbus-java-bin
  * Make sure debug is disabled
  * Depend on and build with sun-java6-jdk

 -- Matthew Johnson <mjj29@debian.org>  Mon, 31 Mar 2008 14:58:34 +0100

dbus-java (2.4-1) unstable; urgency=low

  * New Upstream Release
  * Add build-dep on gettext
  * Build-dep on locale, generate a UTF-8 locale during build and use that
  * Compress using bzip2
  * Depends on version >=0.6 of libunixsocket-java
  * Use Javahelper
  * Bump standards version

 -- Matthew Johnson <mjj29@debian.org>  Tue, 05 Feb 2008 11:49:35 +0000

dbus-java (2.3.2-1) unstable; urgency=low

  * Upstream bug fix release
  * Add watch file
  * Change maintainer address

 -- Matthew Johnson <mjj29@debian.org>  Wed, 05 Dec 2007 09:51:45 +0000

dbus-java (2.3.1-1) unstable; urgency=low

  * Upstream bug fix release (Closes: #422974)
  * Clean up control, add misc:Depends entries
  * Wrap long lines in rules and control
  * Don't ignore make clean errors
  * Call test suite in build
  * Have libdbus-java recommend dbus-java-bin to ensure upgraders don't lose
  functionality
  * Only need to depend on -jre not -jdk as well.
  * Typographic changes to control and rules
  * Call debhelper with -i
  * Build docs under build
  * Fix the licence statement
  * Put a minimum version on the dbus-java-bin - libdbus-java dependency.
  * No need to rm -rf debian/tmp or spare doc files, removed
  * Move from devel to libs and utils

 -- Matthew Johnson <debian@matthew.ath.cx>  Sun,  5 Aug 2007 23:39:40 +0100

dbus-java (2.3-1) unstable; urgency=low

  * New upstream release

 -- Matthew Johnson <debian@matthew.ath.cx>  Fri, 13 Jul 2007 23:38:38 +0100

dbus-java (2.2-2) unstable; urgency=low

  * Change tetex to texlive
  * Update to new Java policy

 -- Matthew Johnson <debian@matthew.ath.cx>  Wed, 30 May 2007 01:27:41 +0100

dbus-java (2.2-1) unstable; urgency=low

  * New Upstream Release

 -- Matthew Johnson <debian@matthew.ath.cx>  Sat, 31 Mar 2007 18:37:49 +0100

dbus-java (2.1-2) unstable; urgency=low

  * Add doc-base files to register the documentation

 -- Matthew Johnson <debian@matthew.ath.cx>  Thu, 11 Jan 2007 12:01:37 +0000

dbus-java (2.1-1) unstable; urgency=low

  * New Upstream Release
  * Remove dependencies on dbus/libdbus
  * Add dependencies on libmatthew-java packages
  * Change source package name and add binary package
  * Add conflicts with old package
  * Add tetex dependencies

 -- Matthew Johnson <debian@matthew.ath.cx>  Thu,  4 Jan 2007 15:35:10 +0000

libdbus-java (1.13-1) unstable; urgency=low

  * New Upstream Release
  * Small changes to the build from upstream
  * Add lintian overrides file
  * Build with full paths to ja{r,va,vac}

 -- Matthew Johnson <debian@matthew.ath.cx>  Wed, 20 Dec 2006 01:40:17 +0000

libdbus-java (1.12-1) unstable; urgency=low

  * New Upstream Release
  * Change dependency to tex4ht from latex2html

 -- Matthew Johnson <debian@matthew.ath.cx>  Wed, 22 Nov 2006 15:28:18 +0000

libdbus-java (1.11-1) unstable; urgency=low

  * New Upstream Release
  * Split binary and doc packages
  * Tidying up of the makefile to use dh_install properly
  * Add a README to /usr/share/doc/libdbus-java-doc to the
    docs location
  * take package description from python/glib packages
  * update 'was downloaded from' link to new location

 -- Matthew Johnson <debian@matthew.ath.cx>  Tue, 14 Nov 2006 16:12:21 +0000

libdbus-java (1.10-1) unstable; urgency=low

  * New Upstream Release

 -- Matthew Johnson <debian@matthew.ath.cx>  Fri,	27 Oct 2006 16:48:58 +0000

libdbus-java (1.8-1) unstable; urgency=low

  * New Upstream Release

 -- Matthew Johnson <debian@matthew.ath.cx>  Thu,  13 Jul 2006 10:04:23 +0000

libdbus-java (1.3-1) unstable; urgency=low

  * New Upstream Release

 -- Matthew Johnson <debian@matthew.ath.cx>  Mon,  22 Feb 2006 10:10:42 +0000

libdbus-java (1.1-1) unstable; urgency=low

  * New Upstream Release

 -- Matthew Johnson <debian@matthew.ath.cx>  Mon,  6 Feb 2006 12:05:23 +0000

libdbus-java (1.0-1) unstable; urgency=low

  * New Upstream Release

 -- Matthew Johnson <debian@matthew.ath.cx>  Mon,  9 Jan 2006 16:39:17 +0000

libdbus-java (0.1) unstable; urgency=low

  * Initial Release.

 -- Matthew Johnson <debian@matthew.ath.cx>  Mon,  19 Dec 2005 15:14:36 +0000

